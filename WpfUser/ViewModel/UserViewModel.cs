﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Collections;

namespace WpfUser.ViewModel
{
    class UserViewModel
    {
        private static string _connectionString;
        private static SqlDataAdapter _adapter;
        private DataTable _usersTable;
        public UserViewModel()
        {
            _usersTable = UpdateUserDataTable();
        }
        public static DataTable UpdateUserDataTable()
        {
            string query = "SELECT * FROM Users;";
            _connectionString = ConfigurationManager.ConnectionStrings["MainConnection"].ConnectionString;
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                SqlCommand command = new SqlCommand(query, sqlConnection);
                _adapter = new SqlDataAdapter(command);
                DataSet dataSet = new DataSet();
                _adapter.Fill(dataSet);
                return dataSet.Tables[0];
            }
        }

        public DataTable Users
        {
            get { return UpdateUserDataTable(); }
        }

        private ICommand _userAddCommand;
        private ICommand _userUpdateCommand;
        private ICommand _userRemoveCommand;
        public ICommand UserAddCommand
        {
            get
            {
                if (_userAddCommand == null)
                    _userAddCommand = new DataManager("Add");
                return _userAddCommand;
            }
            set
            {
                _userAddCommand = value;
            }
        }

        public ICommand UserUpdateCommand
        {
            get
            {
                if (_userUpdateCommand == null)
                    _userUpdateCommand = new DataManager("Update");
                return _userUpdateCommand;
            }
            set
            {
                _userUpdateCommand = value;
            }
        }
        public ICommand UserRemoveCommand
        {
            get
            {
                if (_userRemoveCommand == null)
                    _userRemoveCommand = new DataManager("Remove");
                return _userRemoveCommand;
            }
            set
            {
                _userRemoveCommand = value;
            }
        }

        private class DataManager : ICommand
        {
            private readonly string _type;
            public DataManager()
            {

            }
            public DataManager(string type)
            {
                _type = type;
            }
            #region ICommand Members  

            public bool CanExecute(object parameter)
            {
                return true;
            }

            public event EventHandler CanExecuteChanged;

            public void Execute(object parameter)
            {
                Window window = parameter as Window;
                TextBlock txtId = (TextBlock)window.FindName("txtId");
                TextBox txtFirstName = (TextBox)window.FindName("txtFirstName");
                TextBox txtLastName = (TextBox)window.FindName("txtLastName");
                DatePicker birthdate = (DatePicker)window.FindName("userBirthdate");
                string query = "";
                switch (_type)
                {
                    case "Add":
                        query = "INSERT INTO Users(FirstName, LastName, Birthdate) VALUES('" + txtFirstName.Text + "', '" + txtLastName.Text + "', '" + birthdate.SelectedDate + "'); ";
                        break;
                    case "Update":
                        query = "UPDATE Users SET FirstName = '" + txtFirstName.Text + "', LastName = '" + txtLastName.Text + "', Birthdate = '" + birthdate.SelectedDate + "' WHERE id = " + txtId.Text + "; ";
                        break;
                    case "Remove":
                        query = "DELETE FROM Users WHERE id = " + txtId.Text + "; ";
                        break;
                    default:
                        break;
                }
                using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["MainConnection"].ConnectionString))
                {
                    sqlConnection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(query, sqlConnection);
                    SqlCommand cmd = new SqlCommand(query, sqlConnection);
                    cmd.ExecuteNonQuery();
                }
                DataGrid userGrid = (DataGrid)window.FindName("dataGridUser");
                DataTable dataTable = UserViewModel.UpdateUserDataTable();
                userGrid.ItemsSource = dataTable.DefaultView;
                userGrid.Items.Refresh();
            }

            #endregion
        }


    }

}
